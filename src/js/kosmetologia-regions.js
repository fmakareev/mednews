/**
 * Created by Alex Bogdan on 26.05.2017.
 */
@@include('./assets/all-tabs.js')
@@include('./assets/jquery.autocomplete.js')
@@include('./blocks/drop-activate.js')

$(document).ready(()=>{
    dropActivate('drop');
    tabsActivation('news-tabs','news-tabs__content-item','news-tabs__control-link');
    tabsActivation('problem__contentList','problem__item','problem__toggle');
});
