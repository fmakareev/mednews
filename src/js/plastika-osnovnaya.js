@@include('./assets/placeholder.js')
@@include('./assets/all-tabs.js')
@@include('./assets/jquery.autocomplete.js')
@@include('./blocks/routing.js')
@@include('./assets/city-modal-activate.js')
@@include('./blocks/drop-activate.js')

$(document).ready(()=>{
    dropActivate('drop');
    tabsActivation('news-tabs','news-tabs__content-item','news-tabs__control-link');
    tabsActivation('articles__content','articles__list','articles__item');
});