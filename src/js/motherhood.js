@@include('./vendor/jquery-ui.min.js')
@@include('./assets/slick.js')
@@include('./assets/youtube.js')
@@include('./assets/baron.js')
@@include('./assets/all-tabs.js')
$(document).ready(function() {
    tabsActivation('news-tabs__wrapper','news-tabs__content-item','news-tabs__control-link');
});
$(function () {
    $("#datepicker").datepicker({
        minDate: -20,
        maxDate: "+10D +1M",
        dateFormat: "dd-mm-yy"
    });
});

$("#day_dd").baron({
    root: '.baron-calendar',
    scroller: '.baron-calendar__scroller',
    bar: '.baron-calendar__bar',
    scrollingCls: '_scrolling',
    draggingCls: '_dragging'
});

$("#month_dd").baron({
    root: '.baron-calendar',
    scroller: '.baron-calendar__scroller',
    bar: '.baron-calendar__bar',
    scrollingCls: '_scrolling',
    draggingCls: '_dragging'
});

$("#year_dd").baron({
    root: '.baron-calendar',
    scroller: '.baron-calendar__scroller',
    bar: '.baron-calendar__bar',
    scrollingCls: '_scrolling',
    draggingCls: '_dragging'
});