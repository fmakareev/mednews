@@include('./assets/slick.js')
let slider = $('.page-slider');
slider.slick({
    prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-caret-left" aria-hidden="true"></i></button>',
    nextArrow: '<button type="button" class="slick-next"><i class="fa fa-caret-right" aria-hidden="true"></i></button>',
    adaptiveHeight: true
});