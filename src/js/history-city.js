@@include('./assets/slick.js')

$('.city-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: '<button type="button" class="history-city__slick-prev"><i class="fa fa-caret-left" aria-hidden="true"></i></button>',
    nextArrow: '<button type="button" class="history-city__slick-next"><i class="fa fa-caret-right" aria-hidden="true"></i></button>',
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 1
            }
        }
    ]
});