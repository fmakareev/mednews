@@include('./assets/jquery.autocomplete.js')
@@include('./blocks/routing.js')
@@include('./assets/city-modal-activate.js')
@@include('./assets/all-tabs.js')

$(document).ready(function() {
    tabsActivation('news-tabs','news-tabs__content-item','news-tabs__control-link');
});

@@include('./blocks/drop-activate.js')
$(document).ready(()=>{
    dropActivate('drop');
})

$('.alphabet__item').on('click', function (event) {
    event.preventDefault();
    $('.diseases-block__char').html($(this).text());
})
