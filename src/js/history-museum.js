@@include('./assets/slick.js')

@@include('./assets/jquery.autocomplete.js')
@@include('./blocks/routing.js')
@@include('./assets/city-modal-activate.js')

$('.exhibition__slider-big').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.exhibition__slider-small'
});

$('.exhibition__slider-small').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: '.exhibition__slider-big',
    focusOnSelect: true
});