
@@include('./assets/all-tabs.js')
@@include('./assets/jquery.autocomplete.js')
@@include('./blocks/routing.js')
@@include('./assets/city-modal-activate.js')



$(document).ready(function() {

    tabsActivation('news-tabs','news-tabs__content-item','news-tabs__control-link');
    tabsActivation('tab-container-1','tab-slide-block-1','tab-menu-item-1');
    tabsActivation('tab-container-2','tab-slide-block-2','tab-menu-item-2');
		tabsActivation('tab-container-3','tab-slide-block-3','tab-menu-item-3');
		tabsActivation('tab-container-4','tab-slide-block-4','tab-menu-item-4');
		tabsActivation('tab-container-5','tab-slide-block-5','tab-menu-item-5');
		tabsActivation('tab-container-6','tab-slide-block-6','tab-menu-item-6');
		tabsActivation('tab-container-7','tab-slide-block-7','tab-menu-item-7');
		tabsActivation('tab-container-8','tab-slide-block-8','tab-menu-item-8');
		tabsActivation('tab-container-9','tab-slide-block-9','tab-menu-item-9');
		tabsActivation('tab-container-10','tab-slide-block-10','tab-menu-item-10');
		tabsActivation('tab-container-11','tab-slide-block-11','tab-menu-item-11');
		tabsActivation('tab-container-12','tab-slide-block-12','tab-menu-item-12');
});

