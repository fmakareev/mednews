$(document).ready(function() {
    $('.baron-footer').baron({
        root: '.baron-all',
        scroller: '.baron-all__scroller',
        bar: '.baron-all__bar',
        scrollingCls: '_scrolling',
        draggingCls: '_dragging'
    });
});
