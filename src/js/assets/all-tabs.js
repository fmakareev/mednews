function tabsActivation(wrapperClass,allTabsClass,tabMenuClass) {
    let $wrapper = $(`.${wrapperClass}`),
        $allTabs = $wrapper.find(`.${allTabsClass}`),
        $tabMenu = $wrapper.find(`.${tabMenuClass}`);

    $allTabs.not(':first').hide();
    $tabMenu.each(function(i) {
        $(this).attr('data-tab', 'tab'+i);
    });

    $allTabs.each(function(i) {
        $(this).attr('data-tab', 'tab'+i);
    });

    $tabMenu.on('click', function() {
        event.preventDefault();
        let dataTab = $(this).data('tab'),
            $getWrapper = $(this).closest($wrapper);

        $getWrapper.find($tabMenu).removeClass('active');
        $(this).addClass('active');

        $getWrapper.find($allTabs).hide();
        $getWrapper.find($allTabs).filter('[data-tab='+dataTab+']').show();
    });
}