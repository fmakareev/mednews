$(document).ready(function() {
    let hintSowCounter = 0;
    ymaps.ready(init);
    var footerMap, resortMap;
    function init() {
        // map1
        footerMap = new ymaps.Map("map", {
            center: [59.928347564177365, 30.319548252670277],
            zoom: 16,
            controls: []
        });
        footerMap.behaviors.disable('scrollZoom');
        footerMap.behaviors.disable('drag');
        document.getElementById('map').addEventListener('touchstart', function(event) {
            if (event.targetTouches.length == 2) {
                console.log('2 fingers')
                footerMap.behaviors.enable('drag');
            }else{
                console.log('1 finger')
                hintSowCounter++;
                if(hintSowCounter<4){
                    $('.map__hint').fadeIn(500,()=>{
                        console.log(this);
                        $('.map__hint').fadeOut(3500);
                    });
                }
                footerMap.behaviors.disable('drag');
            }
        }, false);
        let footerCenter = new ymaps.Placemark([59.928369104692955, 30.316200855819694], {
            hintContent: 'Россия, Санкт-Петербург, переулок Гривцова, 20 '
        }, {
            iconLayout: 'default#image',
            iconImageHref: "img/map-marker.png",
            iconImageSize: [89, 89],
            iconImageOffset: [-3, -42]
        });
        footerMap.geoObjects.add(footerCenter);
        // map2
        if (document.getElementById("resortMap")) {
            resortMap = new ymaps.Map("resortMap", {
                center: [44.04984785759731, 43.04088432006828],
                zoom: 13,
                controls: []
            });
            resortMap.behaviors.disable('scrollZoom');
            let resortCenter = new ymaps.Placemark([44.05455239669456, 43.008869473266515], {
                hintContent: 'Пятигорск'
            }, {
                iconLayout: 'default#image',
                iconImageHref: "img/map-marker2.png",
                iconImageSize: [82, 95],
                iconImageOffset: [-30, -50],
            });
            resortMap.geoObjects.add(resortCenter)
        }
    }
    $('.dropdown-selectwrap').each(function() {                 //for all pages
        var btn = $(this).find('.dropdown-select-text');
        var menu = $(this).find('.dropdown-ul');
        var menu_li = $(this).find('.dropdown-ul li');
        btn.text(menu.find('li').first('a').text())

        menu_li.click(function() {
            btn.text($(this).find('a').text());
            $('[data-toggle="dropdown"]').parent().removeClass('open');
            return false;
        });
    });

    if($('.slider')[0]){ //to check .slider for all pages (there are too much in project)
        $('.slider').slick({
            prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-caret-left" aria-hidden="true"></i></button>',
            nextArrow: '<button type="button" class="slick-next"><i class="fa fa-caret-right" aria-hidden="true"></i></button>',
            adaptiveHeight: true
        });
    }
});
