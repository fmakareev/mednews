/**
 * Created by FMakareev on 26.04.2017.
 */
$('.inpSearch').focus(function(){
    $(this).next('.inpSearch__placeholder').hide();
})
$('.inpSearch').blur(function() {
    if ($(this).val().trim() === '') {
        $(this).next('.inpSearch__placeholder').show();
    }
});