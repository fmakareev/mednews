$(document).ready(function(){
    $('body').delegate('a','click',function(event) {
        if (document.getElementById(this.hash.split('#')[1])) {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 500, function(){
                window.location.hash = hash;
            });
        }
    });
});

