/*Created by Alex Bogdan*/
$(document).ready(()=> {
    let loaded = false,
        baronActivate = false,
        btnClicked;
    function defineBtn(btnClass,destId) {

        let btn = $(`.${btnClass}`),
            modal = $('#myModal');
        btn.click((e)=>{
            btnClicked = $(e.currentTarget);
            if (!loaded){
                let url = e.currentTarget.getAttribute('data-url');

                showContent(url,destId);
                loaded = true;
            }
            modal[0].addEventListener("DOMNodeInserted", (ev)=> {
                if (ev.target.className === 'baron baron__root baron__clipper' && !baronActivate){
                    baron({
                        root: '.baron',
                        scroller: '.baron__scroller',
                        bar: '.baron__bar',
                        scrollingCls: '_scrolling',
                        draggingCls: '_dragging'
                    });
                    baronActivate = true;
                    modal.find('.link').click((e)=>{
                        btnClicked.text($(e.currentTarget).text());
                        modal.fadeOut(300);
                        $("html,body").css("overflow-y","auto");
                    });
                    cityAutocomplete();
                }
            }, false);
        })
    }
    defineBtn('city-modal__link','myModal');
    let modal = document.getElementById('myModal'),
        myBtn = $('.city-modal__link'),
        modalCont = $('.city-modal__modal'),
        htmlBody = $("html,body");
    myBtn.click((e)=>{
        $(modal).fadeIn(500);
        htmlBody.css("overflow-y","hidden");
    });
    modalCont.delegate('.closebtn','click',(e)=>{
        modal.style.display = "none";
        htmlBody.css("overflow-y","auto");
    });
    modalCont.delegate('.city-modal__ok-btn','click',(e)=>{
        modal.style.display = "none";
        htmlBody.css("overflow-y","auto");
    });
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
            htmlBody.css("overflow-y","auto");
        }
    };
    function cityAutocomplete() {
        let cities = [
            { value: 'Москва', data: 'AD' },
            { value: 'Санкт-Петербург', data: 'AD' },
            { value: 'Нижневартовск', data: 'AD' },
            { value: 'Саратов', data: 'ZZ' },
            { value: 'Новосибирск', data: 'ZZ' },
            { value: 'Казань', data: 'ZZ' },
            { value: 'Сочи', data: 'ZZ' },
            { value: 'Новосибирск', data: 'ZZ' },
            { value: 'Казань', data: 'ZZ' },
            { value: 'Сочи', data: 'ZZ' },
            { value: 'Уфа', data: 'ZZ' },
            { value: 'Волгоград', data: 'ZZ' },
            { value: 'Пермь', data: 'ZZ' },
            { value: 'Астрахань', data: 'ZZ' },
            { value: 'Воронеж', data: 'ZZ' },
            { value: 'Красноярск', data: 'ZZ' },
            { value: 'Сочи', data: 'ZZ' },
            { value: 'Воронеж', data: 'ZZ' },
            { value: 'Красноярск', data: 'ZZ' },
            { value: 'Сочи', data: 'ZZ' }
        ];
        $('#autocomplete').autocomplete({
            lookup: cities,
            minChars: 2,
            deferRequestBy: 0,
            onSelect: function (suggestion) {
                btnClicked.text(suggestion.value);
            }
        });
    }
});