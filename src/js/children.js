@@include('./assets/baron.js')

$(document).ready(function() {

$("#day_dd").baron({
    root: '.baron-calendar',
    scroller: '.baron-calendar__scroller',
    bar: '.baron-calendar__bar',
    scrollingCls: '_scrolling',
    draggingCls: '_dragging'
});

$("#month_dd").baron({
    root: '.baron-calendar',
    scroller: '.baron-calendar__scroller',
    bar: '.baron-calendar__bar',
    scrollingCls: '_scrolling',
    draggingCls: '_dragging'
});

$("#year_dd").baron({
    root: '.baron-calendar',
    scroller: '.baron-calendar__scroller',
    bar: '.baron-calendar__bar',
    scrollingCls: '_scrolling',
    draggingCls: '_dragging'
});
});