$(document).ready(function() {
 let selectBtns = $('.decret-form__form').find('.dropdown-selectwrap'),
     resetBtn = $('.decret-form__reset');
    resetBtn.click(()=>{
        selectBtns.each((i,item)=>{
            let cont = $(item);
            cont.find('.dropdown-select-text').text(cont.find('.dropdown-menu').find('li')[0].innerText);
        })
    })
});