@@include('./assets/nvslider.js')
@@include('./assets/all-tabs.js')
@@include('./assets/slick.js')
@@include('./assets/youtube.js')
@@include('./assets/jquery.map.js')
@@include('./assets/map.js')
@@include('./blocks/vector-map.js')
@@include('./blocks/drop-activate.js')
@@include('./blocks/interview-activate.js')
$(document).ready(function() {
    tabsActivation('tab-container','tab-slide-block','tab-menu-item');
    dropActivate('drop');
    $('.slider-index').slick({
        infinite: true,
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }

        ]
    });
});