var resortWrap = $('.resort-wrap').html();

function roundAppend() {
    if ($(window).width() >= 991) {
        $('.resort-wrap').html(resortWrap);
    } else {
        $('.resort-wrap__roundwrap').each(function() {
            var test = $(this).find('.resort-wrap__roundtext');
            test.remove();
            $(this).append(test)
        });


    }
}
roundAppend();
$(window).resize(function() {
    roundAppend();
});