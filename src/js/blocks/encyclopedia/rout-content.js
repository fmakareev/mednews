/**
 * Created by Alex Bogdan on 17.04.2017.
 */

$(document).ready(()=>{
    defineLinks('diseases-list-nav__ul-links','dynamic-cont','routing-link');
    showContent('dynamic-blocks/encycl-anatomy.html','dynamic-cont');
});
