$(document).ready(()=>{
    let linksCont = $(".diseases-list-nav__ul-links"),
        destId = 'dynamic-cont';
    determineLinks(linksCont,destId,'rout-link');

    showContent(linksCont.find('a')[0].getAttribute('data-url'),destId,'loading');
});