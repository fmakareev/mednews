/**
 * Created by Alex Bogdan on 05.04.2017.
 */

let listLinks = $( ".med-sec__link" ),
    listDesc = $( ".med-sec__desc-cont" ),
    oops = $('.med-sec__oops');

listLinks.each((i,el)=> {
    $(el).on('click',()=>{
        listLinks.removeClass('_active');
        listDesc.removeClass('visible');
        oops.removeClass('visible');
        if(listDesc[i]){
            listDesc[i].setAttribute('class','med-sec__desc-cont visible');
        }else{
            oops.addClass('visible');
        }
    });
});