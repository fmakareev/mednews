
let buttons = $('.question-all__btn');
buttons.click((e)=>{
    let cont = $(e.currentTarget).closest('.question-all__chess'),
        btnCont = cont.find('.question-all__btn-cont'),
        diagramCont = cont.find('.question-all__diagram-list'),
        hint = cont.find('.question-all__hint');
    hint.removeClass('_dis-none');
    btnCont.css('display','none');
    diagramCont.fadeIn(500);
})