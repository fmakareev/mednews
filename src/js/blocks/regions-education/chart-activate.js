
$(document).ready(()=>{
    $('.reg-universities__diagramm').each((i,elem)=>{
        pieChart($(elem));
    })

});
$(window).resize(()=>{
    $('.reg-universities__diagramm').each((i,elem)=>{
        pieChart($(elem));
    })
})

function pieChart(container) {
    let chart = container.find('.chart')[0];
    $(chart).unbind( "mousemove" );
    $(chart).unbind( "mouseleave" );
    let winWidth = window.innerWidth,
        fontMultiply;
    if (winWidth < 768){
        chart.width = 320;
        chart.height = 320;
        fontMultiply = 0.5;
    }else{
        if(winWidth<1200){
            chart.width =  450;
            chart.height = 450;
            fontMultiply = 0.75;
        } else{
            chart.width =  500;
            chart.height = 500;
            fontMultiply = 1;
        }
    }
    let chartSizePercent = 70,                // Радиус диаграммы, выраженный в процентах от размеров области рисования
        chartInnerRadiusPercent = 20,         // Радиус внутреннего круга в пикселах
        maxPullOutDistance = 25,              // Насколько далеко будет выдвигаться сектор из диаграммы
        pullOutFrameStep = 1,                 // На сколько пикселей Hеперемещается сектор в каждом кадре анимации
        pullOutFrameInterval = 30,            // Сколько ms проходит между кадрами

        valueTextAlign = 'center',
        labelTextAlign = 'center',
        valueFont = "'Trebuchet MS', Verdana, sans-serif",
        labelFont = "'Trebuchet MS', Verdana, sans-serif",
        valueFontSize = 25,
        labelFontSize = 16,
        valueColor ='white',
        labelColor ='white',

        currentPullOutSlice = -1,             // Сектор, который выдвинут в текущий момент(-1 = нет выдвинутого сектора)
        currentPullOutDistance = 0,           // На сколько пикселей смещен текущий выдвигаемый сектор в ходе анимации
        animationId = 0,                      // ID интервала анимации, созданный с помощью setInterval()
        canvasWidth,                          // Ширина области рисования
        canvasHeight,                         // Высота области рисования
        centreX,                              // Координата X центра диаграммы на области рисования
        centreY,                              // Координата Y центра диаграммы на области рисования
        chartRadius,                          // Радиус диаграммы в пикселах
        chartRadiusDinamic,                   // Динамический радиус диаграммы в пикселах
        chartRadiusForPainting,                // Радиус центра линии диаграммы  в пикселах
        chartInnerRadius,                     // Радиус внутреннего круга диаграммы  в пикселах
        charWidth,                            // толщина диаграммы в пикселах
        previousIndex = -1,
        pieceData = [],
        pieceTxtData = [];

    init(chart);
    function init(canvas) {
        if ( typeof canvas.getContext === 'undefined' ) return;
        canvasWidth = canvas.width;
        canvasHeight = canvas.height;
        centreX = canvasWidth / 2;
        centreY = canvasHeight / 2;
        chartInnerRadius = Math.min( canvasWidth, canvasHeight ) / 2 * ( chartInnerRadiusPercent / 100 );
        charWidth = Math.min( canvasWidth, canvasHeight ) / 2 * ( chartSizePercent / 100 ) - chartInnerRadius;
        chartRadiusForPainting = Math.min( canvasWidth, canvasHeight ) / 2 * ( chartSizePercent / 100 ) - charWidth/2;

        chartRadius = Math.min( canvasWidth, canvasHeight ) / 2 * ( chartSizePercent / 100 );
        chartRadiusDinamic = chartRadius;

        let currentPos = 0;
        container.find('.chartData__li').each( function(i) {
            let elem = $(this),
                parentUl = elem.closest('.chartData'),
                percentsSpan = elem.find('.chartData__percents'),
                piecePercentValue = percentsSpan.text(),
                pieceStartAngle = 2 * Math.PI * currentPos,
                pieceEndAngle = 2 * Math.PI * ( currentPos + ( piecePercentValue / 110 ) ),
                pieceColor = percentsSpan.attr('data-bg-color');
            if(i % 2 === 0)
                chartRadiusForPainting-=5;
            else
                chartRadiusForPainting+=5;

            valueTextAlign = parentUl.attr('data-text-align');
            labelTextAlign = parentUl.attr('data-text-align');
            valueFont = parentUl.attr('data-value-font')+",\'Trebuchet MS\', Verdana, sans-serif";  // Шрифт значения выдвинутого сектора
            labelFont = parentUl.attr('data-label-font')+",\'Trebuchet MS\', Verdana, sans-serif";  // Шрифт метки выдвинутого сектора
            valueFontSize = parentUl.attr('data-value-font-size')*fontMultiply;
            labelFontSize = parentUl.attr('data-label-font-size')*fontMultiply;
            valueColor = percentsSpan.attr('data-txt-color');
            labelColor = percentsSpan.attr('data-txt-color');
            let valueTxt = elem.find('.chartData__value').text(),
                labelTxt = elem.find('.chartData__label').html().split("\<br\>"),
                tranlateLeft = percentsSpan.attr('data-left'),
                tranlateTop = percentsSpan.attr('data-top');
            pieceData[i] = [elem,piecePercentValue,pieceStartAngle,pieceEndAngle,pieceColor,chartRadiusForPainting];
            pieceTxtData[i] = [valueTextAlign,labelTextAlign,valueFont,labelFont,valueColor,labelColor,valueTxt,labelTxt,tranlateLeft,tranlateTop,valueFontSize,labelFontSize];
            currentPos += piecePercentValue /100;

        } );
        drawChart();
        $(canvas).mousemove( MouseMoveSwitcher );
        $(canvas).mouseleave( ()=>{
            pushIn();
        });

        function MouseMoveSwitcher(mouseEvent) {
            let mouseX = mouseEvent.pageX - $(this).offset().left,
                mouseY = mouseEvent.pageY - $(this).offset().top,
                xFromCentre = mouseX - centreX,
                yFromCentre = mouseY - centreY,
                distanceFromCentre = Math.sqrt( Math.pow( Math.abs( xFromCentre ), 2 ) + Math.pow( Math.abs( yFromCentre ), 2 ) );
            if ( distanceFromCentre <= chartRadiusDinamic && distanceFromCentre >= chartInnerRadius) {
                let overAngle = Math.atan2( yFromCentre, xFromCentre );
                if ( overAngle < 0 ) overAngle = 2 * Math.PI + overAngle;
                pieceData.forEach((item,i,pieceData)=>{
                    if ( overAngle >= pieceData[i][2] && overAngle <= pieceData[i][3] ) {
                        if(i!==previousIndex){
                            previousIndex = i;
                            togglePiece ( i );
                        }
                        return;
                    }
                });
            }else{
                if(previousIndex!=-1){
                    previousIndex = -1;
                    pushIn();
                }
            }
        }

        function togglePiece ( pieceIndex ) {
            if ( pieceIndex == currentPullOutSlice ) {
                pushIn();
            } else {
                startPullOut ( pieceIndex );
            }
        }
        function startPullOut ( pieceIndex ) {
            if ( currentPullOutSlice == pieceIndex ) return;
            currentPullOutSlice = pieceIndex;
            currentPullOutDistance = 0;
            clearInterval( animationId );
            animationId = setInterval( function() {  animatePullOut( pieceIndex );  }, pullOutFrameInterval );
        }
        function animatePushIn ( pieceIndex ) {
            currentPullOutDistance -= pullOutFrameStep;
            if ( currentPullOutDistance <= 0 ) {
                clearInterval( animationId );
                return;
            }
            drawChart();
        }
        function animatePullOut ( pieceIndex ) {
            currentPullOutDistance += pullOutFrameStep;
            chartRadiusDinamic += pullOutFrameStep;
            if ( currentPullOutDistance >= maxPullOutDistance ) {
                clearInterval( animationId );
                return;
            }
            drawChart();
        }

        function pushIn() {
            chartRadiusDinamic = chartRadius;
            currentPullOutSlice = -1;
            currentPullOutDistance = 0;
            clearInterval( animationId );
            drawChart();
        }

        function drawChart() {
            let context = canvas.getContext('2d');
            context.clearRect ( 0, 0, canvasWidth, canvasHeight );
            pieceData.forEach((item,i,pieceData)=>{
                if ( i != currentPullOutSlice ) drawPiece( context, i );
            });

            if ( currentPullOutSlice != -1 ){
                drawPiece( context, currentPullOutSlice );
            }
        }

        function drawPiece ( context, pieceIndex ) {
            let startAngle = pieceData[pieceIndex][2],
                endAngle = pieceData[pieceIndex][3],
                midAngle,
                startX,
                startY;
            if ( pieceIndex == currentPullOutSlice ) {
                let actualPullOutDistance = currentPullOutDistance * easeOut( currentPullOutDistance/maxPullOutDistance, .8 );
                midAngle = (startAngle + endAngle) / 2;
                startX = centreX + Math.cos(midAngle) * actualPullOutDistance;
                startY = centreY + Math.sin(midAngle) * actualPullOutDistance;
            } else {
                midAngle = (startAngle + endAngle) / 2;
                startX = centreX;
                startY = centreY;
            }
            context.beginPath();
            context.arc( startX, startY, pieceData[pieceIndex][5], startAngle, endAngle, false );
            context.strokeStyle = pieceData[pieceIndex][4];
            context.lineWidth = charWidth;
            context.stroke();

            context.textAlign = pieceTxtData[pieceIndex][0];
            context.font = pieceTxtData[pieceIndex][10]+'px' + pieceTxtData[pieceIndex][2];
            context.fillStyle = pieceTxtData[pieceIndex][4];
            context.fillText ( pieceTxtData[pieceIndex][6], startX + Math.cos(midAngle) * ( pieceData[pieceIndex][5])+Number(pieceTxtData[pieceIndex][8]),Number(pieceTxtData[pieceIndex][9])+  startY + Math.sin(midAngle) * ( chartRadiusForPainting) );

            context.textAlign = pieceTxtData[pieceIndex][1];
            context.font = pieceTxtData[pieceIndex][11]+'px' + pieceTxtData[pieceIndex][3];
            context.fillStyle =  pieceTxtData[pieceIndex][5];
            let marginTop = 0;
            pieceTxtData[pieceIndex][7].forEach((elem,i)=>{
                context.fillText(elem, startX + Math.cos(midAngle) * ( pieceData[pieceIndex][5])+Number(pieceTxtData[pieceIndex][8]),Number(pieceTxtData[pieceIndex][9])+ marginTop*fontMultiply + startY + Math.sin(midAngle) * ( chartRadiusForPainting) + 20*fontMultiply );
                marginTop+=20;
            });
        }
        function easeOut( ratio, power ) {
            return ( Math.pow ( 1 - ratio, power ) + 1 );
        }
    }
}