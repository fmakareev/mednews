$(document).ready(function() {
    let buttons = $('.interview__btn-voice');
    buttons.click((e) => {
        let cont = $(e.currentTarget).closest('.interview__text-content'),
            btnCont = cont.find('.interview__variants'),
            diagramCont = cont.find('.interview__ques-cont-in');
        btnCont.css('display', 'none');
        diagramCont.fadeIn(500);
    });
});