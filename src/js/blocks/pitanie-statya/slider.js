$(document).ready(function() {
    tabsActivation('tab-container','tab-slide-block','tab-menu-item');
    $('.multiple-items').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots:false,
        prevArrow: '<button type="button" class="slider-block__slick-prev"></button>',
        nextArrow: '<button type="button" class="slider-block__slick-next"></button>',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });
});