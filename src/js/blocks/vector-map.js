$(document).ready(function() {
    //map vector
    let colorRegion = '#1076C8'; // Цвет всех регионов
    let focusRegion = '#FF9900'; // Цвет подсветки регионов при наведении на объекты из списка
    let highlighted_states = {};
    $('.vmap').each((i,elem)=>{
        let $elem = $(elem);
        $elem.vectorMap({
            map: 'russia',
            backgroundColor: '#f9f9f9',
            borderColor: '#f9f9f9',
            borderWidth: 2,
            color: colorRegion,
            colors: highlighted_states,
            hoverOpacity: 0.7,
            enableZoom: true,
            showTooltip: true,
            // Клик по региону
            onRegionClick: function(element, code, region) {
            }
        });

        $('.focus-region').mouseover(function() {
            let iso = $(this).prop('id'),
                fregion = {};
            fregion[iso] = focusRegion;
            $elem.vectorMap('set', 'colors', fregion);
        });
        $('.focus-region').mouseout(function() {
            let c = $(this).attr('href'),
                cl = (c === '#') ? colorRegion : c,
                iso = $(this).prop('id'),
                fregion = {};
            fregion[iso] = cl;
            $elem.vectorMap('set', 'colors', fregion);
        });
    })
});