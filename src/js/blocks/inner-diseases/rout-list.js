/**
 * Created by Alex Bogdan on 14.04.2017.
 */


$(document).ready(()=>{
    let baronActivated;
    window.ajaxLoaded = ()=>{
        if (!baronActivated){
            if($('.drop')[0]){
                baronActivated = dropActivate('drop');
            }
        }
    };
    $('.organs-list-cont').delegate('.routing-link','click',(e)=>{
        baronActivated = false;
    });
    defineLinks('mans__img-cont','dynamic-cont','routing-link');
    showContent('dynamic-blocks/mans-popup.html','dynamic-cont');

    defineLinks('organs-list-cont','dynamic-cont-2','mans__link');
    showContent('dynamic-blocks/inner-diseases-dynamic.html','dynamic-cont-2');
});
