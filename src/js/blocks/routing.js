/**
 * Created by Alex Bogdan on 06.04.2017.
 */

    function showContent(link,destId) {

        let cont = document.getElementById(destId);
        let loading = document.getElementById('loading');

        cont.innerHTML = loading.innerHTML;

        let http = createRequestObject();
        if( http )
        {
            http.open('get', link);
            http.onreadystatechange = function ()
            {
                if(http.readyState == 4)
                {
                    cont.innerHTML = http.responseText
                    $(cont).fadeIn(500)
                    let w = window;
                    w.picturefill();
                    if(w.ajaxLoaded)
                        w.ajaxLoaded();
                }
            };
            http.send(null);
        }
        else
        {
            document.location = link;
        }
    }

// создание ajax объекта
    function createRequestObject()
    {
        try { return new XMLHttpRequest() }
        catch(e)
        {
            try { return new ActiveXObject('Msxml2.XMLHTTP') }
            catch(e)
            {
                try { return new ActiveXObject('Microsoft.XMLHTTP') }
                catch(e) { return null; }
            }
        }
    }

    function defineLinks(linksContClass,destId,toggleClass) {
        let linksCont = $(`.${linksContClass}`),
            dest = $(`#${destId}`);
        linksCont.delegate('.routing-link','click',(e)=>{
            let url = e.currentTarget.getAttribute('data-url');
            let link = $(e.currentTarget);
            dest.css('display','none');
            $(`.${toggleClass}`).removeClass('_active');
            link.addClass('_active');
            showContent(url,destId);
            dest.fadeIn(1000);
        })
    }

