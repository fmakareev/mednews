/*created by Alex Bogdan*/

let manBtn = $('.calc-head__man-btn'),
    womanBtn = $('.calc-head__woman-btn'),
    manCont = $('.calc-head__man-cont'),
    womanCont = $('.calc-head__woman-cont');

manBtn.click(()=>{
    womanCont.removeClass('_active');
    womanBtn.removeClass('_active');
    manCont.addClass('_active');
    manBtn.addClass('_active');
});
womanBtn.click(()=>{
    manCont.removeClass('_active');
    manBtn.removeClass('_active');
    womanCont.addClass('_active');
    womanBtn.addClass('_active');
});