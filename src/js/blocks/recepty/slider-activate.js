$(document).ready(function() {
    tabsActivation('tab-container','tab-slide-block','tab-menu-item');
    $('.multiple-items').slick({
        infinite: true,
        slidesToShow: 6,
        slidesToScroll:3,
        dots:false,
        prevArrow: '<button type="button" class="second-dish__slick-prev"></button>',
        nextArrow: '<button type="button" class="second-dish__slick-next"></button>',
        responsive: [{
            breakpoint: 1440,
            settings: {
                slidesToShow: 4,
                slidesToScroll:2
            }
        }, {
            breakpoint: 992,
            settings: {
                slidesToShow: 3,
                slidesToScroll:1
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll:1
            }
        }, {
            breakpoint: 400,
            settings: {
                slidesToShow: 1,
                slidesToScroll:1
            }
        }]
    });
});
