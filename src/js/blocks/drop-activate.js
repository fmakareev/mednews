/**
 * Created by Alex Bogdan on 24.05.2017.
 */

function dropActivate(dropClass) {
    let dropContainers = $('\.'+dropClass);
    dropContainers.each((i,elem)=>{
        let dropContainer = $(elem),
            dropInput = dropContainer.find('.drop__input'),
            dropBtn = dropContainer.find('.drop__toggle'),
            dropUl = dropContainer.find('.drop__items'),
            dropLi = dropContainer.find('.drop__li'),
            liValueArr = [];

        dropUl.hide();
        let closing = 1;
        dropBtn.click(()=>{
            if(closing){
                dropUl.fadeIn(300);
                closing = 0;
            }else{
                dropUl.fadeOut(200);
                closing = 1;
            }
        });
        dropLi.click((event)=>{
            if(!closing){
                let clickedLi = $(event.currentTarget);
                if(dropInput[0])
                    dropInput.val(clickedLi.text());
                else
                    dropBtn.text(clickedLi.text());
                dropUl.fadeOut(200);
                closing = 1;
            }
        });

        $(document).on('click', function(e) {
            if (!$(e.target).closest(elem).length) {
                dropUl.fadeOut(200);
                closing = 1;
            }
            e.stopPropagation();
        });
        if(dropInput[0]){
            dropInput.click(()=>{
                if(!closing){
                    dropUl.fadeOut(200);
                    closing = 1;
                }
            })
            dropInput.keypress(()=>{
                if(!closing){
                    dropUl.fadeOut(200);
                    closing = 1;
                }
            });
            dropInput.keyup(()=>{
                if(!closing){
                    dropUl.fadeOut(200);
                    closing = 1;
                }
            });
            dropLi.each((i,elem)=>{
                liValueArr.push({'value':$(elem).text()});
            });
            inputAutocomplete(liValueArr,dropInput,dropContainer);
        }
        baronActivate(dropUl);
    });



    function inputAutocomplete(valueArr,input,dest) {
        input.autocomplete({
            lookup: valueArr,
            minChars: 2,
            deferRequestBy: 0,
            appendTo: dest
        });
    }
    function baronActivate(elem) {
            elem.baron({
                root: '.baron-all',
                scroller: '.baron-all__scroller',
                bar: '.baron-all__bar',
                scrollingCls: '_scrolling',
                draggingCls: '_dragging'
            });
    }
    return true;
}